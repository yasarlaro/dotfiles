#!/usr/bin/env bash
set -o nounset
set -e

FONTS_DIR="$HOME/.fonts"
path=$(readlink -f "${BASH_SOURCE:-$0}")
DIR_PATH=$(dirname $path)

# Install required fonts
if [ ! -d $FONTS_DIR ]; then
  mkdir $FONTS_DIR
fi

# Add recommended fonts for Oh My Zsh
cp -r $DIR_PATH/.fonts/* $FONTS_DIR/

# Install power10k
if [ ! -d $HOME/powerlevel10k ]; then
  git clone --depth=1 https://github.com/romkatv/powerlevel10k.git $HOME/.powerlevel10k > /dev/null
fi

# Add powerlevel10k to .zshrc 
if ! grep -qw 'source $HOME/.powerlevel10k/powerlevel10k.zsh-theme' $HOME/.zshrc; then
  echo "# Add powerlevel10k.zsh-theme" >> $HOME/.zshrc
  echo 'source $HOME/.powerlevel10k/powerlevel10k.zsh-theme' >> $HOME/.zshrc
fi

# Add az cli autocompletion
mkdir -p $HOME/.zsh
if [ ! -f $HOME/.zsh/az.completion ]; then
  curl -LsS "https://raw.githubusercontent.com/Azure/azure-cli/dev/az.completion" -o $HOME/.zsh/az.completion
fi
if ! grep -qw 'autoload -U +X bashcompinit && bashcompinit' $HOME/.zshrc; then
  echo "# azure-cli auto completion" >> $HOME/.zshrc
  echo "autoload -U +X bashcompinit && bashcompinit" >> $HOME/.zshrc
  echo "source $HOME/.zsh/az.completion" >> $HOME/.zshrc
fi