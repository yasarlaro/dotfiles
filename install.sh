#!/usr/bin/env bash
set -o nounset
set -e

path=$(readlink -f "${BASH_SOURCE:-$0}")
DIR_PATH=$(dirname $path)

# Determine Operating system
if [ -f /etc/redhat-release ]; then
  PACKAGE_MGR="yum"
  DOTSHELL="bash"
  echo "HAS NOT BEEN COMPLETED YET FOR BASH"
  exit 0
fi

if [ -f /etc/lsb-release ]; then
  PACKAGE_MGR="apt"
  DOTSHELL="zsh"
  sudo apt update > /dev/null
fi

# Install required binaries
sudo $PACKAGE_MGR install -y \
  bash \
  zsh \
  wget \
  curl \
  tree \
  git \
  vim \
  python3 \
  libffi-dev \
  openssl \
  bash-completion > /dev/null

# Install kubectl
if ! which kubectl &> /dev/null; then
  mkdir -p ~/.local/bin
  curl -LsS "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" --output ~/.local/bin/kubectl
  chmod +x ~/.local/bin/kubectl
fi

# Install azure-cli
if ! which az &> /dev/null; then 
  if [[ "$PACKAGE_MGR" == "apt" ]]; then
    curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash
  fi
fi

# Install Helm 3
if ! which helm &> /dev/null; then 
  curl -LsS https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
fi

# Run shell specific script
#chmod +x $DIR_PATH/${DOTSHELL}_install.sh
$DIR_PATH/${DOTSHELL}_install.sh